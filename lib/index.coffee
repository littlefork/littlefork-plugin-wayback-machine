url = require 'url'
{join} = require 'path'
_ = require 'lodash'
Promise = require 'bluebird'
request = Promise.promisifyAll require('request')
debug = require('debug')('☈ plugin.wayback')
winston = require 'winston'
moment = require 'moment'

# Documentation:
# http://ws-dl.blogspot.fr/2013/07/2013-07-15-wayback-machine-upgrades.html
TIMEMAPS_URI = 'http://web.archive.org/web/timemap/link/'

# Setup logging.
winston.remove winston.transports.Console
winston.add winston.transports.Console, timestamp: true, colorize: true

get = (url) ->
  request.getAsync url
  .then (response) ->
    if response.statusCode isnt 200
      throw new Error 'HTTP request didn\'t return 200.'
    else response.body

trimUrl = (url) -> _.trimEnd(_.trimStart(url, '<'), '>')
convertDate = (ts) -> moment(new Date(ts)).toISOString()

timegate = (url) ->
  get "#{TIMEMAPS_URI}#{url}"
  .then (body) ->
    _(body.split '\n')
      # Filter no data by excluding lines like ['']
      .reject (line) -> _.isEmpty(line)
      # Get rid of the last comma in the line
      .map (line) -> _.trimEnd(line, ',')
      # parse every single line
      .reduce (memo, line) ->
        [url, fields...] = line.split('; ')

        data = _.reduce fields, (acc, fieldTag) ->
          [key, value] = fieldTag.split('=')
          acc[key] = _.trim(value, '"')
          acc
        , {}

        switch data.rel
          when 'first last memento'
            memo.firstMemento =
              datetime: convertDate data.datetime
              url: trimUrl url
            memo.lastMemento =
              datetime: convertDate data.datetime
              url: trimUrl url
          when 'first memento'
            memo.firstMemento =
              datetime: convertDate data.datetime
              url: trimUrl url
          when 'last memento'
            memo.lastMemento =
              datetime: convertDate data.datetime
              url: trimUrl url
          when 'memento'
            memo.mementos.push(
              datetime: convertDate data.datetime
              url: trimUrl url)
          when 'self'
            memo.self =
              from: convertDate data.from
              until: convertDate data.to
              url: trimUrl url
          when 'timegate'
            memo.timegate = url: trimUrl url
        memo
      , {mementos: []}

module.exports = (envelope) ->
  Promise.map envelope.data, (unit) ->
    urlPath = _(unit._lf_links)
      .filter (link) -> link.type is 'url'
      .map (link) ->
        {host, path} = url.parse link.href
        join host, path
      .first()

    return unit  unless urlPath

    debug "Query time gate for #{urlPath}"

    timegate urlPath
    .then (result) ->
      [snapshot, pubdate] = if _.isEmpty(result)
        [{_lf_wayback: null}, null]
      else
        pubdate = result.firstMemento?.datetime

        [{_lf_wayback: result},
         {_lf_pubdates: _.merge({}, unit._lf_pubdates, {wayback: pubdate})}]

      _.merge unit, snapshot, pubdate
    .delay 1*1000  # miliseconds
  , concurrency: 1
  .then (units) ->
    # Let's create some more stats
    _(units)
      .filter ('_lf_wayback')
      .thru (capturedUnits) ->
        waybackHits: _.size capturedUnits
      .thru (stats) ->
        _.merge envelope, data: units, stats: _.merge(envelope.stats, stats)
      .tap (envelope) ->
        debug "#{envelope.stats.waybackHits} hits on wayback machine."
      .value()
