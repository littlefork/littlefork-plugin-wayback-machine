# littlefork-plugin-wayback-machine

This is a plugin for [littlefork](https://github.com/tacticaltech/littlefork).
It looks up mementos in the
[wayback machine](https://ws-dl.blogspot.fr/2013/07/2013-07-15-wayback-machine-upgrades.html)
of archive.org.

## Installation

```
npm install --save littlefork-plugin-wayback-machine
```

## Usage

This plugin exports a single transformation plugin:

### `wayback-machines` transformation

```
$(npm bin)/littlefork -c cfg.json -p ddg,wayback-machine
```

It adds the `_lf_wayback` field to each data unit that contains all memento
entries. If there are any mementos found, it adds the first one to the
`_lf_pubdates` object with the key `wayback`.
